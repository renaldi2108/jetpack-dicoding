package id.renaldi.jetpackcourse.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.renaldi.jetpackcourse.ui.adapter.DataAdapter
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object SingletonModule {
    @Singleton
    @Provides
    fun provideMovieAdapter(): DataAdapter =
        DataAdapter()
}