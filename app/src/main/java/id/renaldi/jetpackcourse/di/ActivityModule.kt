package id.renaldi.jetpackcourse.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityScoped
import id.renaldi.jetpackcourse.data.repository.FilmRepository
import javax.inject.Singleton

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {
    @Provides
    @ActivityScoped
    fun provideFilmRepository(
        @ApplicationContext context: Context
    ): FilmRepository = FilmRepository(context)
}