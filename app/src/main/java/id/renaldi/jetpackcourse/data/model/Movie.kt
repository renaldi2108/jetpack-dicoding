package id.renaldi.jetpackcourse.data.model

import android.graphics.drawable.Drawable
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

class Movie {
    @Parcelize
    data class Item (
        val title:String,
        val durationItem:String,
        val desc:String,
        val genre:String,
        val img: @RawValue Drawable? = null,
        val years:String,
        val production:String,
    ) : Parcelable
}