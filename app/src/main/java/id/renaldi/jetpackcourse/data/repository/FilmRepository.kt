package id.renaldi.jetpackcourse.data.repository

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.qualifiers.ApplicationContext
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.data.model.Movie
import javax.inject.Inject

class FilmRepository @Inject constructor(@ApplicationContext private val context: Context) {
    fun movieList(): MutableLiveData<List<Movie.Item>> {
        val title = context.resources.getStringArray(R.array.movieTitle)
        val durationItem = context.resources.getStringArray(R.array.movieTime)
        val desc = context.resources.getStringArray(R.array.movieDesc)
        val genre = context.resources.getStringArray(R.array.movieGenre)
        val img = context.resources.obtainTypedArray(R.array.movieImg)
        val years = context.resources.getStringArray(R.array.movieYears)
        val production = context.resources.getStringArray(R.array.movieProduction)

        val items = mutableListOf<Movie.Item>()
        val itemsLD = MutableLiveData<List<Movie.Item>>()

        for(size in 0..9) {
            items.add(Movie.Item(title[size], durationItem[size],
                desc[size], genre[size], img.getDrawable(size), years[size], production[size]
            ))
        }

        itemsLD.value = items

        return itemsLD
    }

    @SuppressLint("Recycle")
    fun tvList(): MutableLiveData<List<Movie.Item>> {
        val title = context.resources.getStringArray(R.array.tvTitle)
        val durationItem = context.resources.getStringArray(R.array.tvTime)
        val desc = context.resources.getStringArray(R.array.tvDesc)
        val genre = context.resources.getStringArray(R.array.tvGenre)
        val img = context.resources.obtainTypedArray(R.array.tvImg)
        val years = context.resources.getStringArray(R.array.tvYears)
        val production = context.resources.getStringArray(R.array.tvProduction)

        val items = mutableListOf<Movie.Item>()
        val itemsLD = MutableLiveData<List<Movie.Item>>()

        for(size in 0..9) {
            items.add(Movie.Item(title[size], durationItem[size],
                desc[size], genre[size], img.getDrawable(size), years[size], production[size]
            ))
        }

        itemsLD.value = items

        return itemsLD
    }
}