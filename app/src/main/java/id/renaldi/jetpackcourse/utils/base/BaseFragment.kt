package id.renaldi.jetpackcourse.utils.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import id.renaldi.jetpackcourse.utils.data.autoCleared

abstract class BaseFragment<T : ViewDataBinding> : Fragment(), BaseView, LifecycleObserver {
    protected var binding: T by autoCleared()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding =
                DataBindingUtil.inflate(inflater, getLayoutResource(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
        initData()
    }

    override fun initViews() = Unit

    override fun initObservers() = Unit

    override fun initData() = Unit
}