package id.renaldi.jetpackcourse.utils.base

import androidx.annotation.LayoutRes

interface BaseView {
    @LayoutRes
    fun getLayoutResource(): Int
    fun initViews()
    fun initObservers()
    fun initData()
}