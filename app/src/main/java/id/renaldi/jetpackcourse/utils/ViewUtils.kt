package id.renaldi.jetpackcourse.utils

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

inline val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun ImageView.loadImage(
    url: String = "",
    drawable: Drawable?,
    roundCorner: Int = 0,
    makeItCircle: Boolean = false,
    placeholder: Int? = null,
    onLoadDone:(()->Unit)? = null
) {
    val builder = Glide.with(this)
        .load(if(url.isNotEmpty()) url else drawable)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
    placeholder?.let { builder.placeholder(it) }

    if (makeItCircle) builder.circleCrop()
    else if (roundCorner > 0) builder.transform(CenterCrop(), RoundedCorners(roundCorner.dp))

    if(onLoadDone!=null) {
        builder.addListener(object : RequestListener<Drawable>{
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                onLoadDone.invoke()
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                onLoadDone.invoke()
                return false
            }

        }).into(this)
    } else{
        builder.into(this)
    }
}