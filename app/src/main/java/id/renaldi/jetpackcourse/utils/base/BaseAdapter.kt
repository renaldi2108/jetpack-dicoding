package id.renaldi.jetpackcourse.utils.base

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter <T: Any>: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val items: MutableList<T> = mutableListOf()

    fun data(position: Int): T = items[position]

    abstract fun enableClick(onItemClickListener: OnItemClickListener<T>)

    fun add(items: List<T>) {
        for(item in items) {
            add(item)
        }
    }

    fun add(item: T) {
        items.add(item)
        notifyItemInserted(items.size)
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    abstract override fun getItemViewType(position: Int): Int
    abstract override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
}

interface OnItemClickListener<T> {
    fun onItemClick(item: T)
}