package id.renaldi.jetpackcourse.ui.main.movie

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.data.model.Movie
import id.renaldi.jetpackcourse.databinding.FragmentMovieBinding
import id.renaldi.jetpackcourse.ui.adapter.DataAdapter
import id.renaldi.jetpackcourse.utils.base.BaseFragment
import id.renaldi.jetpackcourse.utils.base.OnItemClickListener
import javax.inject.Inject

@AndroidEntryPoint
class MovieFragment: BaseFragment<FragmentMovieBinding>(), OnItemClickListener<Movie.Item> {
    private val viewModel: MovieViewModel by viewModels()
    @Inject lateinit var adapter: DataAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_movie

    override fun initViews() {
        super.initViews()
        adapter.clear()
        binding.recyclerView.adapter = adapter

        adapter.enableClick(this)
    }

    override fun initObservers() {
        super.initObservers()
        viewModel.movie().observe(viewLifecycleOwner) {
            adapter.add(it)
        }
    }

    override fun onItemClick(item: Movie.Item) {
        findNavController().navigate(
            MovieFragmentDirections.actionMovieShowFragmentToDetailFragment(item)
        )
    }
}