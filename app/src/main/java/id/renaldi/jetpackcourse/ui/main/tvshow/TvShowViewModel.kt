package id.renaldi.jetpackcourse.ui.main.tvshow

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.renaldi.jetpackcourse.data.repository.FilmRepository
import javax.inject.Inject

@HiltViewModel
class TvShowViewModel @Inject constructor(private val repository: FilmRepository): ViewModel() {
    fun tvShow() = repository.tvList()
}