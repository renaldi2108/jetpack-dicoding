package id.renaldi.jetpackcourse.ui.main.movie

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.renaldi.jetpackcourse.data.repository.FilmRepository
import javax.inject.Inject


@HiltViewModel
class MovieViewModel @Inject constructor(private val repository: FilmRepository): ViewModel() {
    fun movie() = repository.movieList()
}