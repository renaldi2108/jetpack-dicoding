package id.renaldi.jetpackcourse.ui.detail

import androidx.navigation.fragment.navArgs
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.data.model.Movie
import id.renaldi.jetpackcourse.databinding.FragmentDetailBinding
import id.renaldi.jetpackcourse.utils.base.BaseFragment
import id.renaldi.jetpackcourse.utils.loadImage

class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    private val args: DetailFragmentArgs by navArgs()
    private val item: Movie.Item by lazy { args.movie }

    override fun getLayoutResource(): Int = R.layout.fragment_detail

    override fun initViews() {
        super.initViews()
        binding.ivImage.loadImage(drawable = item.img)
        binding.tvStoryline.text = item.desc
        binding.tvRuntime.text = item.durationItem
        binding.tvYears.text = item.years
        binding.tvGenre.text = item.genre
        binding.tvProduction.text = item.production
    }
}