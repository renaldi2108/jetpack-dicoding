package id.renaldi.jetpackcourse.ui.main.tvshow

import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.data.model.Movie
import id.renaldi.jetpackcourse.databinding.FragmentTvshowBinding
import id.renaldi.jetpackcourse.ui.adapter.DataAdapter
import id.renaldi.jetpackcourse.ui.main.movie.MovieFragmentDirections
import id.renaldi.jetpackcourse.utils.base.BaseFragment
import id.renaldi.jetpackcourse.utils.base.OnItemClickListener
import javax.inject.Inject

@AndroidEntryPoint
class TvShowFragment : BaseFragment<FragmentTvshowBinding>(), OnItemClickListener<Movie.Item> {

    private val viewModel: TvShowViewModel by viewModels()
    @Inject lateinit var adapter: DataAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_tvshow

    override fun initViews() {
        super.initViews()
        adapter.clear()
        binding.recyclerView.adapter = adapter

        adapter.enableClick(this)
    }

    override fun initObservers() {
        super.initObservers()
        viewModel.tvShow().observe(viewLifecycleOwner) {
            adapter.add(it)
        }
    }

    override fun onItemClick(item: Movie.Item) {
        findNavController().navigate(
            TvShowFragmentDirections.actionTvShowFragmentToDetailFragment(item)
        )
    }
}