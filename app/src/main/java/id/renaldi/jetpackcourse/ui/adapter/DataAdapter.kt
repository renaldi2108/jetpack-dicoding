package id.renaldi.jetpackcourse.ui.adapter

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.data.model.Movie
import id.renaldi.jetpackcourse.databinding.ItemMovieTvBinding
import id.renaldi.jetpackcourse.utils.base.BaseAdapter
import id.renaldi.jetpackcourse.utils.base.OnItemClickListener
import id.renaldi.jetpackcourse.utils.getBindingOf
import id.renaldi.jetpackcourse.utils.loadImage

class DataAdapter: BaseAdapter<Movie.Item>() {
    private var _onItemClickListener: OnItemClickListener<Movie.Item>?= null
    private val onItemClickListener: OnItemClickListener<Movie.Item> get() = _onItemClickListener!!
    override fun getItemViewType(position: Int): Int = R.layout.item_movie_tv

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): RecyclerView.ViewHolder =
        ItemViewHolder(
            parent.getBindingOf(viewType)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data(position)

        when (holder) {
            is ItemViewHolder -> {
                holder.binding.tvTitle.text = item.title
                holder.binding.tvDesc.text = item.desc
                holder.binding.tvDuration.text = item.durationItem
                holder.itemView.setOnClickListener {
                    onItemClickListener.onItemClick(item)
                }
                holder.binding.ivThumb.loadImage(drawable = item.img)
            }
        }
    }

    class ItemViewHolder(val binding: ItemMovieTvBinding): RecyclerView.ViewHolder(binding.root)

    override fun enableClick(onItemClickListener: OnItemClickListener<Movie.Item>) {
        _onItemClickListener = onItemClickListener
    }
}