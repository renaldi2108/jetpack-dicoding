package id.renaldi.jetpackcourse.ui.main

import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import id.renaldi.jetpackcourse.R
import id.renaldi.jetpackcourse.databinding.ActivityMainBinding
import id.renaldi.jetpackcourse.utils.base.BaseActivity

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    private lateinit var navController: NavController

    override fun getLayoutResource(): Int = R.layout.activity_main

    override fun initViews() {
        super.initViews()

        val navHostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController

        binding.bottomAppBar.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            binding.bottomAppBar.isVisible = content.contains(destination.id)
            if(content.contains(destination.id))
                binding.bottomAppBar.navigationIcon = null
        }

        binding.navView.setOnNavigationItemSelectedListener {item->
            NavigationUI.onNavDestinationSelected(item, navController)
            true
        }
    }

    private val content = listOf(
        R.id.movieFragment,
        R.id.tvShowFragment
    )
}