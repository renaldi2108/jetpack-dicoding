package id.renaldi.jetpackcourse.data

import id.renaldi.jetpackcourse.data.source.DataDummy
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Assert
import org.junit.Test

class DataDummyTest {

    private val hellowthere: DataDummy = mockk()

    @Test
    fun `hellow`() {
        mockkStatic(DataDummy::class)
        val expectedValue = "Jetpack Course"

        every { hellowthere.hellow() } returns expectedValue

        Assert.assertEquals(expectedValue, hellowthere.hellow())
    }
}